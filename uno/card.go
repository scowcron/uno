package uno

import (
	"errors"
	"strconv"
)

const (
	NONE   = iota
	ACTION = iota
	COLOR  = iota
)

const (
	WILD        = "Wild"
	WILD_DRAW_4 = "Wild / Draw 4"
)

const (
	RED    = "Red"
	BLUE   = "Blue"
	GREEN  = "Green"
	YELLOW = "Yellow"
)

const (
	NUMERIC = "number"
	SKIP    = "Skip"
	DRAW_2  = "Draw 2"
	REVERSE = "Reverse"
)

type Card struct {
	Type    int
	Action  string
	Color   string
	Numeric bool
	Value   int
}

func (c *Card) String() string {
	if c.Type == NONE {
		return "None"
	}

	if c.Type == ACTION {
		if c.Action == WILD_DRAW_4 {
			return "wd4: " + c.Action
		} else {
			return "w: " + c.Action
		}
	}

	var full, short string
	switch c.Color {
	case RED:
		short = "r"
	case BLUE:
		short = "b"
	case YELLOW:
		short = "y"
	case GREEN:
		short = "g"
	}

	if c.Numeric {
		n := strconv.Itoa(c.Value)
		full = c.Color + " " + n
		short += n
	} else {
		switch c.Action {
		case REVERSE:
			short += "r"
		case DRAW_2:
			short += "d2"
		case SKIP:
			short += "s"
		}
		full = c.Color + " " + c.Action
	}
	return short + ": " + full
}

func (c *Card) ShortString() string {
	if c.Type == NONE {
		return ""
	}

	if c.Type == ACTION {
		if c.Action == WILD_DRAW_4 {
			return "wd4"
		} else {
			return "w"
		}
	}

	var ret string
	switch c.Color {
	case RED:
		ret = "r"
	case BLUE:
		ret = "b"
	case YELLOW:
		ret = "y"
	case GREEN:
		ret = "g"
	}

	if c.Numeric {
		ret += strconv.Itoa(c.Value)
	} else {
		switch c.Action {
		case REVERSE:
			ret += "r"
		case DRAW_2:
			ret += "d2"
		case SKIP:
			ret += "s"
		}
	}

	return ret
}

func StringToCard(s string) (Card, error) {
	switch s {
	case "w":
		return Card{Type: ACTION, Action: WILD}, nil
	case "wd4":
		return Card{Type: ACTION, Action: WILD_DRAW_4}, nil
	}

	c, v := s[0], s[1:]
	var color string
	switch c {
	case 'r':
		color = RED
	case 'g':
		color = GREEN
	case 'b':
		color = BLUE
	case 'y':
		color = YELLOW
	default:
		return Card{}, errors.New("Unrecognized color")
	}

	switch v {
	case "r":
		return Card{Type: COLOR, Color: color, Action: REVERSE}, nil
	case "s":
		return Card{Type: COLOR, Color: color, Action: SKIP}, nil
	case "d2":
		return Card{Type: COLOR, Color: color, Action: DRAW_2}, nil
	}

	value, err := strconv.Atoi(v)
	if err != nil {
		return Card{}, err
	}

	return Card{Type: COLOR, Color: color, Numeric: true, Value: value}, nil
}
