package uno

import "errors"

type Game struct {
	reversed bool
	turn     int
	hands    []Hand
	deck     *Deck
	stack    []Card
	over     bool
}

func NewGame(nplayers int) (*Game, error) {
	g := &Game{over: false}

	if nplayers < 2 || nplayers > 9 {
		return nil, errors.New("Must be between 2 and 9 players")
	}

	g.deck = NewDeck()
	g.deck.Shuffle()
	g.stack = append(g.stack, g.deck.Draw())

	for i := 0; i < nplayers; i++ {
		hand := Hand{Id: i}
		for j := 0; j < 7; j++ {
			hand.Cards = append(hand.Cards, g.deck.Draw())
		}
		g.hands = append(g.hands, hand)
	}

	return g, nil
}

func (g *Game) NumPlayers() (int, error) {
	if g.over {
		return 0, errors.New("Game is over")
	}
	return len(g.hands), nil
}

func (g *Game) CurrentHand() (Hand, error) {
	if g.over {
		return Hand{}, errors.New("Game is over")
	}

	return g.hands[g.turn], nil
}

func (g *Game) TopCard() (Card, error) {
	if g.over {
		return Card{}, errors.New("Game is over")
	}

	if len(g.stack) == 0 {
		return Card{Type: NONE}, nil
	}

	return g.stack[len(g.stack)-1], nil
}

func (g *Game) endTurn() Hand {
	if g.reversed {
		g.turn = g.turn - 1 + len(g.hands)
	} else {
		g.turn = g.turn + 1
	}
	g.turn %= len(g.hands)
	return g.hands[g.turn]
}

func (g *Game) Draw() (Hand, Card, error) {
	if g.over {
		return Hand{}, Card{}, errors.New("Game is over")
	}

	if len(g.deck.Cards) == 0 {
		g.deck.Cards = g.stack[:len(g.stack)-1]
		g.stack = []Card{g.deck.Cards[len(g.stack)-1]}
		g.deck.Shuffle()
	}

	c := g.deck.Draw()
	g.hands[g.turn].Cards = append(g.hands[g.turn].Cards, c)
	nh := g.endTurn()
	return nh, c, nil
}

func (g *Game) Take() (Hand, Card, error) {
	if g.over {
		return Hand{}, Card{}, errors.New("Game is over")
	}

	if len(g.stack) == 0 {
		return g.hands[g.turn], Card{Type: NONE}, errors.New("No card on stack.")
	}

	c := g.stack[len(g.stack)-1]
	g.stack = g.stack[:len(g.stack)-1]
	nh := g.endTurn()
	return nh, c, nil
}

func (g *Game) Play(c Card) (Hand, error) {
	if g.over {
		return Hand{}, errors.New("Game is over")
	}

	ch := &g.hands[g.turn]

	idx := -1
	for i, card := range ch.Cards {
		if card == c {
			idx = i
			break
		}
	}
	if idx == -1 {
		return Hand{Id: -1}, errors.New("Card not in hand")
	}

	ch.Cards = append(ch.Cards[:idx], ch.Cards[idx+1:]...)
	g.stack = append(g.stack, c)
	nh := g.endTurn()

	if len(ch.Cards) == 0 {
		g.over = true
	}

	return nh, nil
}

func (g *Game) Turn() (int, error) {
	if g.over {
		return 0, errors.New("Game is over")
	}
	return g.turn, nil
}

func (g *Game) Hand(id int) (Hand, error) {
	if id < 0 || id > len(g.hands) {
		return Hand{Id: -1}, errors.New("No such player")
	}
	return g.hands[id], nil
}
