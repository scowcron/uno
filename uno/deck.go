package uno

import (
	"math/rand"
	"time"
)

func init() {
	rand.Seed(time.Now().UTC().UnixNano())
}

type Deck struct {
	Cards []Card
}

func NewDeck() *Deck {
	d := &Deck{}

	// 4 each of wild, wild/draw 4
	for i := 0; i < 4; i++ {
		d.Cards = append(d.Cards, Card{Type: ACTION, Action: WILD})
		d.Cards = append(d.Cards, Card{Type: ACTION, Action: WILD_DRAW_4})
	}

	colors := [...]string{RED, BLUE, YELLOW, GREEN}
	for _, color := range colors {

		for i := 0; i < 2; i++ {
			d.Cards = append(d.Cards, Card{Type: COLOR, Color: color, Action: SKIP})
			d.Cards = append(d.Cards, Card{Type: COLOR, Color: color, Action: DRAW_2})
			d.Cards = append(d.Cards, Card{Type: COLOR, Color: color, Action: REVERSE})
		}

		for i := 0; i < 10; i++ {
			d.Cards = append(d.Cards, Card{Type: COLOR, Color: color, Numeric: true, Value: i})
		}
		for i := 1; i < 10; i++ {
			d.Cards = append(d.Cards, Card{Type: COLOR, Color: color, Numeric: true, Value: i})
		}

	}

	return d
}

// Fisher-Yates shuffle
func (d *Deck) Shuffle() {
	o := rand.Perm(len(d.Cards))
	n := make([]Card, 108)
	copy(n, d.Cards)
	for i := 0; i < len(o); i++ {
		d.Cards[i] = n[o[i]]
	}
}

func (d *Deck) Draw() Card {
	c := d.Cards[len(d.Cards)-1]
	d.Cards = d.Cards[:len(d.Cards)-1]
	return c
}
