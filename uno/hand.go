package uno

type Hand struct {
	Id    int
	Cards []Card
}

func (h *Hand) Print() {
	println("Hand", h.Id)
	for _, c := range h.Cards {
		println(" ", c.String())
	}
}
