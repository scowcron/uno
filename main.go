package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"

	"./api"
)


func main() {
	host := flag.String("host", "localhost", "hostname to listen on")
	port := flag.Int("port", 2023, "port to listen on")
	flag.Parse()

	serve := api.NewServer()
	http.Handle("/", serve)

	listen := fmt.Sprintf("%s:%d", *host, *port)

	log.Fatal(http.ListenAndServe(listen, nil))
}
