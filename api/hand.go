package api

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
)

type GetHandRequest struct {
	HandID string `json:"hid"`
}

type GetHandResponse struct {
	Ok    bool     `json:"ok"`
	Cards []string `json:"cards"`
}

func (srv *Server) getHand(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	raw, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, ErrorBody("Failed to read response"), 500)
		return
	}

	var ghr GetHandRequest
	err = json.Unmarshal(raw, &ghr)
	if err != nil {
		http.Error(w, ErrorBody(err.Error()), 405)
		return
	}

	hme, ok := srv.hands[ghr.HandID]
	if !ok {
		http.Error(w, ErrorBody("No such hand"), 405)
		return
	}

	h, _ := srv.games[hme.Game].Hand(hme.Hand)
	resp := GetHandResponse{Ok: true}
	for _, c := range h.Cards {
		s := c.ShortString()
		if s != "" {
			resp.Cards = append(resp.Cards, s)
		}
	}

	s, _ := json.Marshal(resp)
	io.WriteString(w, string(s)+"\n")
}

func (srv *Server) handHandlerFunc(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		srv.getHand(w, r)
	default:
		http.Error(w, "Method not allowed", 400)
	}
}
