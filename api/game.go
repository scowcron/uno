package api

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"math/rand"
	"net/http"
	"strconv"

	"../uno"
)

type NewGameRequest struct {
	Players int `json:"players"`
}

type DeleteGameRequest struct {
	GameID int `json:"gid"`
}

type NewGameResponse struct {
	Ok      bool     `json:"ok"`
	GameID  int      `json:"gid"`
	HandIDs []string `json:"hids"`
}

type GamesResponse struct {
	Ok    bool  `json:"ok"`
	Games []int `json:"gids"`
}

type DeleteGameResponse struct {
	Ok bool `json:"ok"`
}

func (srv *Server) newHandID() string {
	k := ""
	ok := true
	for ok {
		for i := 0; i < 10; i++ {
			k += strconv.Itoa(rand.Intn(10))
		}
		_, ok = srv.hands[k]
	}
	return k
}

func (srv *Server) setupNewGame(r NewGameRequest) (string, error) {
	game, err := uno.NewGame(r.Players)
	if err != nil {
		return "", err
	}

	gid := srv.nextGame
	srv.games[gid] = game
	srv.nextGame++

	resp := NewGameResponse{Ok: true, GameID: gid}
	for i := 0; i < r.Players; i++ {
		e := HandMapEntry{Game: gid, Hand: i}
		hid := srv.newHandID()
		srv.hands[hid] = e
		srv.handGids[e] = hid
		resp.HandIDs = append(resp.HandIDs, hid)
	}

	s, _ := json.Marshal(resp)
	return string(s), nil
}

func (srv *Server) getGame(w http.ResponseWriter, r *http.Request) {
	resp := GamesResponse{Ok: true, Games: make([]int, 0)}
	for gid, _ := range srv.games {
		resp.Games = append(resp.Games, gid)
	}

	s, _ := json.Marshal(resp)
	io.WriteString(w, string(s)+"\n")
}

func (srv *Server) postGame(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, ErrorBody("Failed to read response"), 500)
		return
	}

	var conf NewGameRequest
	err = json.Unmarshal(body, &conf)
	if err != nil {
		http.Error(w, ErrorBody(err.Error()), 405)
		return
	}

	_, err = uno.NewGame(conf.Players)
	if err != nil {
		http.Error(w, ErrorBody(err.Error()), 405)
		return
	}

	resp, err := srv.setupNewGame(conf)
	if err != nil {
		http.Error(w, ErrorBody(err.Error()), 405)
		return
	}

	io.WriteString(w, resp+"\n")
}

func (srv *Server)  deleteGame(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, ErrorBody("Failed to read response"), 500)
		return
	}

	var dgr DeleteGameRequest
	err = json.Unmarshal(body, &dgr)
	if err != nil {
		http.Error(w, ErrorBody(err.Error()), 405)
		return
	}

	_, ok := srv.games[dgr.GameID]
	if !ok {
		http.Error(w, ErrorBody("Invalid game"), 405)
		return
	}

	delete(srv.games, dgr.GameID)
	for i, h := range srv.hands {
		delete(srv.hands, i)
		delete(srv.handGids, h)
	}
	dr := DeleteGameResponse{Ok: true}
	s, _ := json.Marshal(dr)
	io.WriteString(w, string(s)+"\n")

}

func (srv *Server) gameHandlerFunc(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		srv.getGame(w, r)
	case "POST":
		srv.postGame(w, r)
	case "DELETE":
		srv.deleteGame(w, r)
	default:
		http.Error(w, "Method not allowed", 400)
	}
}
