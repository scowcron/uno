package api

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
)

type StackRequest struct {
	Game int `json:"gid"`
}

type StackResponse struct {
	Ok   bool   `json:"ok"`
	Card string `json:"card"`
}

func (srv *Server) stackHandlerFunc(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	raw, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, ErrorBody(err.Error()), 405)
		return
	}

	var sr StackRequest
	err = json.Unmarshal(raw, &sr)
	if err != nil {
		http.Error(w, ErrorBody(err.Error()), 405)
		return
	}

	g := srv.games[sr.Game]
	c, err := g.TopCard()
	if err != nil {
		http.Error(w, ErrorBody(err.Error()), 405)
		return
	}

	resp := StackResponse{Ok: true, Card: c.ShortString()}
	s, _ := json.Marshal(resp)
	io.WriteString(w, string(s)+"\n")
}
