package api

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"

	"../uno"
)

const (
	PLAY = "play"
	DRAW = "draw"
	TAKE = "take"
)

type ActionRequest struct {
	Hand   string `json:"hid"`
	Action string `json:"act"`
	Card   string `json:"card"`
}

type DrawResponse struct {
	Ok   bool   `json:"ok"`
	Card string `json:"card"`
}

type TakeResponse struct {
	Ok   bool   `json:"ok"`
	Card string `json:"card"`
}

type PlayResponse struct {
	Ok   bool   `json:"ok"`
	Next string `json:"next"`
}

func (srv *Server) drawAction(w http.ResponseWriter, gid int) {
	g := srv.games[gid]
	_, c, err := g.Draw()
	if err != nil {
		http.Error(w, ErrorBody(err.Error()), 405)
	}
	r := DrawResponse{Ok: true, Card: c.ShortString()}
	s, _ := json.Marshal(r)
	io.WriteString(w, string(s)+"\n")
}

func (srv *Server) takeAction(w http.ResponseWriter, gid int) {
	g := srv.games[gid]
	_, c, err := g.Take()
	if err != nil {
		http.Error(w, ErrorBody(err.Error()), 405)
		return
	}

	r := TakeResponse{Ok: true, Card: c.ShortString()}
	s, _ := json.Marshal(r)
	io.WriteString(w, string(s)+"\n")
}

func (srv *Server) playAction(w http.ResponseWriter, gid int, c string) {
	g := srv.games[gid]
	card, err := uno.StringToCard(c)
	if err != nil {
		http.Error(w, ErrorBody(err.Error()), 405)
		return
	}

	_, err = g.Play(card)
	if err != nil {
		http.Error(w, ErrorBody(err.Error()), 405)
		return
	}

	t, err := g.Turn()
	if err != nil {
		http.Error(w, ErrorBody(err.Error()), 405)
		return
	}

	nh := HandMapEntry{Game: gid, Hand: t}

	r := PlayResponse{Ok: true, Next: srv.handGids[nh]}
	s, _ := json.Marshal(r)
	io.WriteString(w, string(s)+"\n")
}

func (srv *Server) actionHandlerFunc(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.Error(w, "Method not allowed", 400)
		return
	}

	defer r.Body.Close()
	raw, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, ErrorBody(err.Error()), 405)
		return
	}

	var ar ActionRequest
	err = json.Unmarshal(raw, &ar)
	if err != nil {
		http.Error(w, ErrorBody(err.Error()), 405)
		return
	}

	h, ok := srv.hands[ar.Hand]
	if !ok {
		http.Error(w, ErrorBody("Invalid hand"), 405)
		return
	}

	g, ok := srv.games[h.Game]
	if !ok {
		http.Error(w, ErrorBody("Failed to find game"), 500)
		return
	}

	t, err := g.Turn()
	if err != nil {
		http.Error(w, ErrorBody(err.Error()), 405)
		return
	}

	if h.Hand != t {
		http.Error(w, ErrorBody("It's not your turn"), 405)
		return
	}

	switch ar.Action {
	case DRAW:
		srv.drawAction(w, h.Game)
	case TAKE:
		srv.takeAction(w, h.Game)
	case PLAY:
		srv.playAction(w, h.Game, ar.Card)
	default:
		http.Error(w, ErrorBody("Unrecognized action"), 405)
	}
}
