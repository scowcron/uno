package api

import (
	"net/http"

	"../uno"
)

type HandMapEntry struct {
	Game int
	Hand int
}

type Server struct {
	games map[int]*uno.Game
	hands map[string]HandMapEntry
	handGids map[HandMapEntry]string
	nextGame int

	mux *http.ServeMux
}

func NewServer() *Server {
	s := &Server{}
	s.games = make(map[int]*uno.Game, 0)
	s.hands = make(map[string]HandMapEntry, 0)
	s.handGids = make(map[HandMapEntry]string)

	s.mux = http.NewServeMux()
	s.mux.HandleFunc("/game", s.gameHandlerFunc)
	s.mux.HandleFunc("/hand", s.handHandlerFunc)
	s.mux.HandleFunc("/act", s.actionHandlerFunc)
	s.mux.HandleFunc("/stack", s.stackHandlerFunc)

	return s
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.mux.ServeHTTP(w, r)
}
