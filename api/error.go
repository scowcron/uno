package api

import "encoding/json"

type ErrorResponse struct {
	Ok      bool
	Message string
}

func ErrorBody(text string) string {
	e := ErrorResponse{
		Ok:      false,
		Message: text,
	}

	r, _ := json.Marshal(e)
	return string(r)
}
